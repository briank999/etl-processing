﻿using System;
using GemBox.Pdf;
using GemBox.Pdf.Content;

namespace PDFGenTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // If using Professional version, put your serial key below.
            ComponentInfo.SetLicense("FREE-LIMITED-KEY");

// NB The strings that are processed here, have a limit of 500 Characters, despite the incoming database columns being capable of up to 2G size (VARCHAR(MAX))

            string DocRef = "MEDIDECC7123100002";
            string EmployeeName = "Brian Kitchen";
            string ClockNumber = "102999";
            string Descr = "This is the Description        with a long description just to test how the text wraps round onto the next line. This part might be on another line but you never know. The third line could start anyway in this string, but you will still never know. The fourth line could be the same, that could appear on the fourth line you just never know. And yet again there could also be a fifth line, and you will never know which line it will appear on. Then yet again there could be a sixth sentence in this tex";
            string MedCon = "This is the medical condition with a long description just to test how the text wraps round onto the next line.This part might be on another line but you never know. The third line could start anyway in this string, but you will still never know. The fourth line could be the same, that could appear on the fourth line you just never know. And yet again there could also be a fifth line, and you will never know which line it will appear on. Then yet again there could be a sixth sentence in this tex";
            string MedTaken = "This is the medication taken with a long description just to test how the text wraps round onto the next line. This part might be on another line but you never know. The third line could start anyway in this string, but you will still never know. The fourth line could be the same, that could appear on the fourth line you just never know. And yet again there could also be a fifth line, and you will never know which line it will appear on. Then yet again there could be a sixth sentence in this tex";
            string SideEffects = "These are the side effects with along description just to test how the text wraps round onto the next line. This part might be on another line but you never know. The third line could start anyway in this string, but you will still never know. The fourth line could be the same, that could appear on the fourth line you just never know. And yet again there could also be a fifth line, and you will never know which line it will appear on. Then yet again there could be a sixth sentence in this tex";
            string TimeScale = "This is the Time Scale      with a long description just to test how the text wraps round onto the next line. This part might be on another line but you never know. The third line could start anyway in this string, but you will still never know. The fourth line could be the same, that could appear on the fourth line you just never know. And yet again there could also be a fifth line, and you will never know which line it will appear on. Then yet again there could be a sixth sentence in this tex";
            string GPReview = "This is the GP Review        with a long description just to test how the text wraps round onto the next line. This part might be on another line but you never know. The third line could start anyway in this string, but you will still never know. The fourth line could be the same, that could appear on the fourth line you just never know. And yet again there could also be a fifth line, and you will never know which line it will appear on. Then yet again there could be a sixth sentence in this tex";
            string Line;
            int strlen;

            using (var document = new PdfDocument())
            {
                // Add a page.
                var page = document.Pages.Add();

                // Write a text.
                using (var formattedText = new PdfFormattedText())
                {
                    formattedText.TextAlignment = PdfTextAlignment.Center;
                    formattedText.FontFamily = new PdfFontFamily("Calibri");
                    formattedText.FontSize = 16;
                    string Title = string.Concat("Medical Declaration Form for Employee ", EmployeeName, " Clock Number: ", ClockNumber);
                    formattedText.AppendLine(Title);

                    formattedText.AppendLine("");

                    strlen = Descr.Length;
                    formattedText.TextAlignment = PdfTextAlignment.Left;
                    formattedText.FontFamily = new PdfFontFamily("Calibri");
                    formattedText.FontSize = 12;
                    formattedText.FontWeight = PdfFontWeight.Bold;
                    Line = "Description:";
                    formattedText.AppendLine(Line);

                    formattedText.FontWeight = PdfFontWeight.Normal;
                    if (strlen < 90)
                    {
                        Line = Descr.Substring(0, strlen);
                        formattedText.AppendLine(Line);
                    }
                    else
                    {
                        Line = Descr.Substring(0, 90);
                        formattedText.AppendLine(Line);
                        if (strlen < 180)
                        {
                            Line = Descr.Substring(90, ((strlen - 90) - 1));
                            formattedText.AppendLine(Line);
                        }
                        else
                        {
                            Line = Descr.Substring(90, 90);
                            formattedText.AppendLine(Line);
                            if (strlen < 270)
                            {
                                Line = Descr.Substring(180, ((strlen - 180) - 1));
                                formattedText.AppendLine(Line);
                            }
                            else
                            {
                                Line = Descr.Substring(180, 90);
                                formattedText.AppendLine(Line);
                                if (strlen < 360)
                                {
                                    Line = Descr.Substring(270, ((strlen - 360) - 1));
                                    formattedText.AppendLine(Line);
                                }
                                else
                                {
                                    Line = Descr.Substring(270, 90);
                                    formattedText.AppendLine(Line);
                                    if (strlen < 450)
                                    {
                                        Line = Descr.Substring(360, ((strlen - 450) - 1));
                                        formattedText.AppendLine(Line);
                                    }
                                    else
                                    {
                                        Line = Descr.Substring(360, 90);
                                        formattedText.AppendLine(Line);
                                        if (strlen < 501)
                                        {
                                            Line = Descr.Substring(450, ((strlen - 450) - 1));
                                            formattedText.AppendLine(Line);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    formattedText.AppendLine("");

                    strlen = MedCon.Length;
                    formattedText.TextAlignment = PdfTextAlignment.Left;
                    formattedText.FontFamily = new PdfFontFamily("Calibri");
                    formattedText.FontSize = 12;
                    formattedText.FontWeight = PdfFontWeight.Bold;
                    Line = "Medical Condition:";
                    formattedText.AppendLine(Line);

                    formattedText.FontWeight = PdfFontWeight.Normal;

                    if (strlen < 90)
                    {
                        Line = MedCon.Substring(0, strlen);
                        formattedText.AppendLine(Line);
                    }
                    else
                    {
                        Line = MedCon.Substring(0, 90);
                        formattedText.AppendLine(Line);
                        if (strlen < 180)
                        {
                            Line = MedCon.Substring(90, ((strlen - 90) -1 ) );
                            formattedText.AppendLine(Line);
                        }
                        else
                        {
                            Line = MedCon.Substring(90, 90);
                            formattedText.AppendLine(Line);
                            if (strlen < 270)
                            {
                                Line = MedCon.Substring(180, ((strlen - 180) - 1));
                                formattedText.AppendLine(Line);
                            }
                            else
                            {
                                Line = MedCon.Substring(180, 90);
                                formattedText.AppendLine(Line);
                                if (strlen < 360)
                                {
                                    Line = MedCon.Substring(270, ((strlen - 360) - 1));
                                    formattedText.AppendLine(Line);
                                }
                                else
                                {
                                    Line = MedCon.Substring(270, 90);
                                    formattedText.AppendLine(Line);
                                    if (strlen < 450)
                                    {
                                        Line = MedCon.Substring(360, ((strlen - 450) - 1));
                                        formattedText.AppendLine(Line);
                                    }
                                    else
                                    {
                                        Line = MedCon.Substring(360, 90);
                                        formattedText.AppendLine(Line);
                                        if (strlen < 501)
                                        {
                                            Line = MedCon.Substring(450, ((strlen - 450) - 1));
                                            formattedText.AppendLine(Line);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    formattedText.AppendLine("");

                    strlen = MedTaken.Length;
                    formattedText.TextAlignment = PdfTextAlignment.Left;
                    formattedText.FontFamily = new PdfFontFamily("Calibri");
                    formattedText.FontSize = 12;
                    formattedText.FontWeight = PdfFontWeight.Bold;
                    Line = "Medication Being Taken:";
                    formattedText.AppendLine(Line);

                    formattedText.FontWeight = PdfFontWeight.Normal;
                    if (strlen < 90)
                    {
                        Line = MedTaken.Substring(0, strlen);
                        formattedText.AppendLine(Line);
                    }
                    else
                    {
                        Line = MedTaken.Substring(0, 90);
                        formattedText.AppendLine(Line);
                        if (strlen < 180)
                        {
                            Line = MedTaken.Substring(90, ((strlen - 90) - 1));
                            formattedText.AppendLine(Line);
                        }
                        else
                        {
                            Line = MedTaken.Substring(90, 90);
                            formattedText.AppendLine(Line);
                            if (strlen < 270)
                            {
                                Line = MedTaken.Substring(180, ((strlen - 180) - 1));
                                formattedText.AppendLine(Line);
                            }
                            else
                            {
                                Line = MedTaken.Substring(180, 90);
                                formattedText.AppendLine(Line);
                                if (strlen < 360)
                                {
                                    Line = MedTaken.Substring(270, ((strlen - 360) - 1));
                                    formattedText.AppendLine(Line);
                                }
                                else
                                {
                                    Line = MedTaken.Substring(270, 90);
                                    formattedText.AppendLine(Line);
                                    if (strlen < 450)
                                    {
                                        Line = MedTaken.Substring(360, ((strlen - 450) - 1));
                                        formattedText.AppendLine(Line);
                                    }
                                    else
                                    {
                                        Line = MedTaken.Substring(360, 90);
                                        formattedText.AppendLine(Line);
                                        if (strlen < 501)
                                        {
                                            Line = MedTaken.Substring(450, ((strlen - 450) - 1));
                                            formattedText.AppendLine(Line);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    formattedText.AppendLine("");

                    strlen = SideEffects.Length;
                    formattedText.TextAlignment = PdfTextAlignment.Left;
                    formattedText.FontFamily = new PdfFontFamily("Calibri");
                    formattedText.FontSize = 12;
                    formattedText.FontWeight = PdfFontWeight.Bold;
                    Line = "Side Effects:";
                    formattedText.AppendLine(Line);

                    formattedText.FontWeight = PdfFontWeight.Normal;
                    if (strlen < 90)
                    {
                        Line = SideEffects.Substring(0, strlen);
                        formattedText.AppendLine(Line);
                    }
                    else
                    {
                        Line = SideEffects.Substring(0, 90);
                        formattedText.AppendLine(Line);
                        if (strlen < 180)
                        {
                            Line = SideEffects.Substring(90, ((strlen - 90) - 1));
                            formattedText.AppendLine(Line);
                        }
                        else
                        {
                            Line = SideEffects.Substring(90, 90);
                            formattedText.AppendLine(Line);
                            if (strlen < 270)
                            {
                                Line = SideEffects.Substring(180, ((strlen - 180) - 1));
                                formattedText.AppendLine(Line);
                            }
                            else
                            {
                                Line = SideEffects.Substring(180, 90);
                                formattedText.AppendLine(Line);
                                if (strlen < 360)
                                {
                                    Line = SideEffects.Substring(270, ((strlen - 360) - 1));
                                    formattedText.AppendLine(Line);
                                }
                                else
                                {
                                    Line = SideEffects.Substring(270, 90);
                                    formattedText.AppendLine(Line);
                                    if (strlen < 450)
                                    {
                                        Line = SideEffects.Substring(360, ((strlen - 450) - 1));
                                        formattedText.AppendLine(Line);
                                    }
                                    else
                                    {
                                        Line = SideEffects.Substring(360, 90);
                                        formattedText.AppendLine(Line);
                                        if (strlen < 501)
                                        {
                                            Line = SideEffects.Substring(450, ((strlen - 450) -1));
                                            formattedText.AppendLine(Line);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    formattedText.AppendLine("");

                    strlen = TimeScale.Length;
                    formattedText.TextAlignment = PdfTextAlignment.Left;
                    formattedText.FontFamily = new PdfFontFamily("Calibri");
                    formattedText.FontSize = 12;
                    formattedText.FontWeight = PdfFontWeight.Bold;
                    Line = "How long the medication will be taken for:";
                    formattedText.AppendLine(Line);

                    formattedText.FontWeight = PdfFontWeight.Normal;
                    if (strlen < 90)
                    {
                        Line = TimeScale.Substring(0, strlen);
                        formattedText.AppendLine(Line);
                    }
                    else
                    {
                        Line = TimeScale.Substring(0, 90);
                        formattedText.AppendLine(Line);
                        if (strlen < 180)
                        {
                            Line = TimeScale.Substring(90, ((strlen - 90) - 1));
                            formattedText.AppendLine(Line);
                        }
                        else
                        {
                            Line = TimeScale.Substring(90, 90);
                            formattedText.AppendLine(Line);
                            if (strlen < 270)
                            {
                                Line = TimeScale.Substring(180, ((strlen - 180) - 1));
                                formattedText.AppendLine(Line);
                            }
                            else
                            {
                                Line = TimeScale.Substring(180, 90);
                                formattedText.AppendLine(Line);
                                if (strlen < 360)
                                {
                                    Line = TimeScale.Substring(270, ((strlen - 360) - 1));
                                    formattedText.AppendLine(Line);
                                }
                                else
                                {
                                    Line = TimeScale.Substring(270, 90);
                                    formattedText.AppendLine(Line);
                                    if (strlen < 450)
                                    {
                                        Line = TimeScale.Substring(360, ((strlen - 450) - 1));
                                        formattedText.AppendLine(Line);
                                    }
                                    else
                                    {
                                        Line = TimeScale.Substring(360, 90);
                                        formattedText.AppendLine(Line);
                                        if (strlen < 501)
                                        {
                                            Line = TimeScale.Substring(450, ((strlen - 450) - 1));
                                            formattedText.AppendLine(Line);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    formattedText.AppendLine("");

                    strlen = GPReview.Length;
                    formattedText.TextAlignment = PdfTextAlignment.Left;
                    formattedText.FontFamily = new PdfFontFamily("Calibri");
                    formattedText.FontSize = 12;
                    formattedText.FontWeight = PdfFontWeight.Bold;
                    Line = "Details of next GP Review:";
                    formattedText.AppendLine(Line);

                    formattedText.FontWeight = PdfFontWeight.Normal;
                    if (strlen < 90)
                    {
                        Line = GPReview.Substring(0, strlen);
                        formattedText.AppendLine(Line);
                    }
                    else
                    {
                        Line = GPReview.Substring(0, 90);
                        formattedText.AppendLine(Line);
                        if (strlen < 180)
                        {
                            Line = GPReview.Substring(90, ((strlen - 90) - 1));
                            formattedText.AppendLine(Line);
                        }
                        else
                        {
                            Line = GPReview.Substring(90, 90);
                            formattedText.AppendLine(Line);
                            if (strlen < 270)
                            {
                                Line = GPReview.Substring(180, ((strlen - 180) - 1));
                                formattedText.AppendLine(Line);
                            }
                            else
                            {
                                Line = GPReview.Substring(180, 90);
                                formattedText.AppendLine(Line);
                                if (strlen < 360)
                                {
                                    Line = GPReview.Substring(270, ((strlen - 360) - 1));
                                    formattedText.AppendLine(Line);
                                }
                                else
                                {
                                    Line = GPReview.Substring(270, 90);
                                    formattedText.AppendLine(Line);
                                    if (strlen < 450)
                                    {
                                        Line = GPReview.Substring(360, ((strlen - 450) - 1));
                                        formattedText.AppendLine(Line);
                                    }
                                    else
                                    {
                                        Line = GPReview.Substring(360, 90);
                                        formattedText.AppendLine(Line);
                                        if (strlen < 501)
                                        {
                                            Line = GPReview.Substring(450, ((strlen - 450) - 1));
                                            formattedText.AppendLine(Line);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    page.Content.DrawText(formattedText, new PdfPoint(0,50));
                }

                document.Save("C:\\Users\\KIT91420\\Documents\\Visual Studio 2019\\Hello World.pdf");
            }
            Console.WriteLine("Hello World!");
        }
    }
}
